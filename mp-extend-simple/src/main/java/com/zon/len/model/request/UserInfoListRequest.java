package com.zon.len.model.request;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zon.len.base.AbstractListRequest;
import com.zon.len.model.po.UserInfoPo;
import lombok.Getter;
import lombok.Setter;

/**
 * -请求列表
 * @author ZonLen since on 2021-12-18 16:02:38
 */

@Getter
@Setter
public class UserInfoListRequest extends AbstractListRequest {

  @Override
  public LambdaQueryWrapper<UserInfoPo> wrapper() {
    final LambdaQueryWrapper<UserInfoPo> wrapper = new LambdaQueryWrapper<>(UserInfoPo.class);
    //TODO 添加条件
    return wrapper;
  }
}

