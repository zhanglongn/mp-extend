package com.zon.len.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zon.len.base.AbstractPo;
import com.zon.len.context.ApplicationContextProvider;
import com.zon.len.model.po.UserInfoPo.UserInfoTableShardingStrategy;
import com.zon.len.mp.extend.sharding.TableSharding;
import com.zon.len.mp.extend.sharding.AbstractTableShardingStrategy;
import lombok.Getter;
import lombok.Setter;

/**
 * 持久化
 *
 * @author ZonLen since on 2021-12-18 16:02:37
 */

@Getter
@Setter
@TableName("user_info")
//@TableSharding(sharding = UserInfoTableShardingStrategy.class)
public class UserInfoPo extends AbstractPo {

  private String userName;
  private Integer age;

  public static class UserInfoTableShardingStrategy extends
      AbstractTableShardingStrategy<UserInfoPo> {

    @Override
    public String dataBaseName() {
      return ApplicationContextProvider.getProperty("mp.extend.simple.user_db");
    }

    @Override
    public String shardingTableName(String orgTableName) {
      return orgTableName;
    }
  }
}

