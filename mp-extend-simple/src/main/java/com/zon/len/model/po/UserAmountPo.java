package com.zon.len.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zon.len.base.AbstractPo;
import com.zon.len.context.ApplicationContextProvider;
import com.zon.len.model.po.UserAmountPo.UserAmountTableShardingStrategy;
import com.zon.len.mp.extend.sharding.TableSharding;
import com.zon.len.mp.extend.sharding.AbstractTableShardingStrategy;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.Setter;

/**
 * -持久化
 *
 * @author ZonLen since on 2021-12-18 22:00:49
 */

@Getter
@Setter
@TableName("user_amount")
@TableSharding(sharding = UserAmountTableShardingStrategy.class)
public class UserAmountPo extends AbstractPo {

  private Long userId;

  private BigDecimal amount;
  /**
   * 删除标识(0:未删除，1:已删除) <dr(java.lang.Long)>
   */
  private Long dr;

  public static class UserAmountTableShardingStrategy extends
      AbstractTableShardingStrategy<UserAmountPo> {

    @Override
    public String dataBaseName() {
      return ApplicationContextProvider.getProperty("mp.extend.simple.user_db");
    }

    @Override
    public String shardingTableName(String orgTableName) {
      return orgTableName + 1;
    }
  }
}

