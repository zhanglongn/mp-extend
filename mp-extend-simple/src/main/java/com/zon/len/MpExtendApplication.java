package com.zon.len;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ZonLen since on 2021/12/23 下午3:35
 */
@SpringBootApplication
public class MpExtendApplication {

  public static void main(String[] args) {
    SpringApplication.run(MpExtendApplication.class, args);
  }

}
