package com.zon.len.context;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author ZonLen since on 2021/12/24 下午3:09
 */
public class ApplicationContextProvider implements
    ApplicationContextInitializer<ConfigurableApplicationContext> {

  private static ConfigurableApplicationContext applicationContext;

  @Override
  public void initialize(ConfigurableApplicationContext applicationContext) {
    ApplicationContextProvider.applicationContext = applicationContext;
  }

  public static String getProperty(String placeHolder) {
    return applicationContext.getEnvironment().getProperty(placeHolder, StringPool.EMPTY);
  }

}
