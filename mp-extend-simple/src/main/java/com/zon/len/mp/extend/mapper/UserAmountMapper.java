package com.zon.len.mp.extend.mapper;

import com.zon.len.model.po.UserAmountPo;
import org.apache.ibatis.annotations.Mapper;

/**
 * -Mapper
 * @author ZonLen since on 2021-12-18 22:00:50
 */
@Mapper
public interface UserAmountMapper extends ExtendBaseMapper<UserAmountPo> {
}

