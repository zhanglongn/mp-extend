package com.zon.len.mp.extend.mapper;

import com.zon.len.model.po.UserInfoPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * -Mapper
 *
 * @author ZonLen since on 2021-12-18 16:02:37
 */
@Mapper
public interface UserInfoMapper extends ExtendBaseMapper<UserInfoPo> {

  long countUser(@Param("userInfoId") String userInfoId, @Param("age") Integer age, @Param("amount") Integer amount);
}

