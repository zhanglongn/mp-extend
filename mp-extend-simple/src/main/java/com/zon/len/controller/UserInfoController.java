package com.zon.len.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zon.len.model.po.UserInfoPo.UserInfoTableShardingStrategy;
import com.zon.len.dynamic.annocation.DynamicDataSource;
import com.zon.len.dynamic.DynamicDruidDataSource.DynamicDataSourceKey;
import com.zon.len.mp.extend.mapper.UserInfoMapper;
import com.zon.len.model.po.UserAmountPo;
import com.zon.len.model.po.UserAmountPo.UserAmountTableShardingStrategy;
import com.zon.len.model.po.UserInfoPo;
import com.zon.len.mp.extend.wrapper.LambdaExtendWrapper;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * -表现
 *
 * @author ZonLen since on 2021-12-18 16:02:32
 */

@RestController
public class UserInfoController {

  private final UserInfoMapper userInfoMapper;

  public UserInfoController(UserInfoMapper userInfoMapper) {
    this.userInfoMapper = userInfoMapper;
  }


  @DynamicDataSource(DynamicDataSourceKey.DEMO_DB1)
  @GetMapping("/user/info/list")
  public List<UserInfoPo> loadUserInfoList() {
    final LambdaExtendWrapper<UserInfoPo> wrapper = new LambdaExtendWrapper<>(UserInfoPo.class);
    wrapper
        .eq(UserInfoPo::getId, 1)
        .rightJoin(UserAmountPo.class, joinQuery -> joinQuery
            .eq(UserAmountPo::getUserId, UserInfoPo::getId)
            .or(innerJoinQuery -> innerJoinQuery.eq(UserAmountPo::getAmount, 100)
                .eq(UserAmountPo::getUserId, UserInfoPo::getId))
//            COUNT(distinct "id") AS 'total'
//            .select(UserAmountPo::getId, "total", AggregationFunction.COUNT_DISTINCT)
            .select(UserAmountPo.class, null)
            .shardingStrategy(new UserAmountTableShardingStrategy()))
        .eq(UserInfoPo::getAge, 31)
//        .eq(UserAmountPo.class, UserAmountPo::getAmount, 100);
        .shardingStrategy(new UserInfoTableShardingStrategy());

    System.out.println("-------LambdaExtendWrapper------joinSelectList-");
    userInfoMapper.joinSelectList(wrapper);
    System.out.println("---------LambdaExtendWrapper----joinSelectCount-");
    userInfoMapper.joinSelectCount(wrapper);
//    System.out.println("----------LambdaExtendWrapper----joinSelectOne");
//    userInfoMapper.joinSelectOne(wrapper);
    System.out.println("----------LambdaExtendWrapper----joinSelectPage");
    userInfoMapper.joinSelectPage(new Page<>(1, 2), wrapper);
    System.out.println("-----------LambdaExtendWrapper---selectList");
    userInfoMapper.selectList(wrapper);
    System.out.println("----------LambdaExtendWrapper---selectCount-");
    userInfoMapper.selectCount(wrapper);
    System.out.println("----------LambdaExtendWrapper----selectPage");
    userInfoMapper.selectPage(new Page<>(2, 2), wrapper);
    System.out.println("----------LambdaExtendWrapper----selectMaps");
    userInfoMapper.selectMaps(wrapper);
    System.out.println("----------LambdaExtendWrapper----selectMapsPage");
    userInfoMapper.selectMapsPage(new Page<>(2, 2), wrapper);
//    System.out.println("----------LambdaExtendWrapper----selectOne");
//    userInfoMapper.selectOne(wrapper);

    final LambdaQueryWrapper<UserInfoPo> query = Wrappers.lambdaQuery();
    query.eq(UserInfoPo::getId, 1).eq(UserInfoPo::getAge, 31);
    System.out.println("-------LambdaQueryWrapper-------selectList");
    userInfoMapper.selectList(query);
//    System.out.println("--------LambdaQueryWrapper------selectOne");
//    userInfoMapper.selectOne(query);
    System.out.println("---------LambdaQueryWrapper-----selectPage");
    userInfoMapper.selectPage(new Page<>(2, 2), query);
    System.out.println("---------LambdaQueryWrapper-----selectCount");
    userInfoMapper.selectCount(query);
    return new ArrayList<>();
  }


}

