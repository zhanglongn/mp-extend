package com.zon.len.base;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * 持久化对象抽象类
 *
 * @author ZonLen since on 2021/9/10 下午8:27
 */

public abstract class AbstractPo {

  public AbstractPo() {
  }

  /**
   * 主键自增
   */
  @Getter
  @Setter
  @TableId(type = IdType.AUTO)
  private Long id;
  /**
   * 创建时间
   */
  @Getter
  @Setter
  private Date creationTime;
  /**
   * 创建人
   */
  @Getter
  @Setter
  private String creator;

  @Getter
  @Setter
  @TableField(exist = false)
  private String createName;
  /**
   * 时间戳
   */
  @Getter
  @Setter
  private Date pubts;
  /**
   * 更新人
   */
  @Getter
  @Setter
  private String modifier;
  /**
   * 更新时间
   */
  @Getter
  @Setter
  private Date modifiedTime;
  /**
   * 删除标识
   */
  @Getter
  @Setter
  private Long dr;
  /**
   * 租户ID
   */
  @Getter
  @Setter
  private String ytenantId;


}
