package com.zon.len.base;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.Setter;

/**
 * 列表查询导出功能使用
 *
 * @author ZonLen since on 2021/9/10 下午9:40
 */

public abstract class AbstractListRequest implements BusinessRequest {

  /**
   * 默认 10
   */
  @Setter
  @Getter
  private int size = 15;
  /**
   * 默认 1
   */
  @Setter
  @Getter
  private int current = 1;


  public <PO> Page<PO> page() {
    return new Page<>(current, size);
  }

}
