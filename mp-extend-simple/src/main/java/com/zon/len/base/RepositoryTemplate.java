package com.zon.len.base;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

/**
 * @author ZonLen since on 2021/9/10 下午8:20
 */
public interface RepositoryTemplate<P extends AbstractPo> extends IService<P> {

  List<P> loadListByIdsIfEmptyThrow(Set<? extends Serializable> ids, String errorMsg);

  List<P> loadListByConditionIfEmptyThrow(Consumer<LambdaQueryWrapper<P>> consumer,
      String errorMsg);

  P loadOneByIdIfEmptyThrow(Serializable id, String errorMsg);

  P loadOneByConditionIfEmptyThrow(Consumer<LambdaQueryWrapper<P>> consumer, String errorMsg);

  P loadOneByCondition(Consumer<LambdaQueryWrapper<P>> consumer);

  P loadOneByWrapperIfEmptyThrow(LambdaQueryWrapper<P> wrapper, String errorMsg);

  P loadOneByWrapper(LambdaQueryWrapper<P> wrapper);

  List<P> loadListByWrapperIfEmptyThrow(LambdaQueryWrapper<P> wrapper, String errorMsg);

  List<P> loadListByWrapper(LambdaQueryWrapper<P> wrapper);

  List<P> loadListByCondition(Consumer<LambdaQueryWrapper<P>> consumer);

  void addBatch(List<P> pList);

  void add(P po);

  void updateByCondition(Consumer<LambdaUpdateWrapper<P>> consumer);

  void update(LambdaUpdateWrapper<P> wrapper);

  boolean existByCondition(Consumer<LambdaQueryWrapper<P>> consumer);

  boolean existByWrapper(Wrapper<P> wrapper);

  Class<P> pClass();

  Page<P> loadPage(Page<P> page, LambdaQueryWrapper<P> wrapper);

  void logicDeleteByCondition(Consumer<LambdaUpdateWrapper<P>> consumer);
}
