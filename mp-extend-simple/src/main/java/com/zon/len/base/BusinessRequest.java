package com.zon.len.base;

import com.baomidou.mybatisplus.core.conditions.Wrapper;

/**
 * @author ZonLen since on 2021/9/10 下午9:40
 */
public interface BusinessRequest {


  /**
   * 查询条件
   */
  <PO extends AbstractPo> Wrapper<PO> wrapper();

  /**
   * 租户ID
   */
//  default String getYtenantId() {
//    CasCorp corp = InvocationInfoUtils.getEntityAttr(CasCorp.class);
//    if (null == corp) {
//      return null;
//    } else {
//      return corp.getCorpId();
//    }
//  }

  default Long getDr() {
    return 0L;
  }

}
