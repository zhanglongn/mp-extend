package com.zon.len.mp.extend.script;

/**
 * @author ZonLen since on 2021/12/20 下午8:25
 */
public interface MybatisCommand {

  String getMethod();

  String getSql();
}
