package com.zon.len.mp.extend.method;

import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.zon.len.mp.extend.script.ExtendConstant;
import com.zon.len.mp.extend.script.ExtendMybatisCommand;
import com.zon.len.mp.extend.script.MybatisCommand;

/**
 * @author ZonLen since on 2021/12/18 下午5:14
 */
public class ExtendJoinSelectCount extends AbstractJoinMethod {

  @Override
  public String sqlFormat(TableInfo tableInfo) {
    return String
        .format(sqlCommandType().getSql(), sqlFirst(),
            tableNamePlaceHolder(tableInfo.getTableName()), ExtendConstant.JOIN_SQL_NAME,
            sqlWhereEntityWrapper(true, tableInfo),
            sqlComment());
  }

  @Override
  public Class<?> resultType(TableInfo tableInfo) {
    return Long.class;
  }

  @Override
  public MybatisCommand sqlCommandType() {
    return ExtendMybatisCommand.JOIN_SELECT_COUNT;
  }

}
