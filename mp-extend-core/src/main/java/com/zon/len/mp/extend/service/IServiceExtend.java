package com.zon.len.mp.extend.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zon.len.mp.extend.wrapper.LambdaExtendWrapper;
import java.util.List;

/**
 * @author ZL since on 2021/12/18 下午9:29
 */
public interface IServiceExtend<T> extends IService<T> {


  /**
   * 查询列表
   *
   * @param wrapper 实体对象封装操作类
   * @param <E>     返回泛型（如果只查询一个字段可以传递String Int之类的类型）
   * @return 返回E 类型的列表
   */
  <EV, E> List<EV> joinList(LambdaExtendWrapper<E> wrapper, Class<EV> clz);

  /**
   * 查询单个对象
   *
   * @param wrapper 实体对象封装操作类
   * @param clz     返回对象 （如果只查询一个字段可以传递String Int之类的类型）
   * @param <E>     包装泛型类型
   * @param <EV>    返回类型泛型
   * @return EV
   */
  <E, EV> EV joinGetOne(LambdaExtendWrapper<E> wrapper, Class<EV> clz);


  /**
   * 查询count
   *
   * @param wrapper 实体对象封装操作类
   * @param <E>     返回泛型
   * @return 总数
   */
  <E> long joinCount(LambdaExtendWrapper<E> wrapper);


  /**
   * 翻页查询
   *
   * @param page    翻页对象
   * @param wrapper 实体对象封装操作类
   */
  <EV, E extends IPage<EV>, C> IPage<EV> joinPage(E page, LambdaExtendWrapper<C> wrapper,
      Class<EV> clz);

}
