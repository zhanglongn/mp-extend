package com.zon.len.mp.extend.script;

/**
 * @author ZonLen since on 2021/12/27 下午4:36
 */
public enum AggregationFunction {

  SIMPLE("%s"),
  COUNT("COUNT(%s)"),
  COUNT_DISTINCT("COUNT(DISTINCT %s)"),
  SUM("SUM(%s)"),
  MIN("MIN(%s)"),
  MAX("MAX(%s)");

  private final String function;

  AggregationFunction(String function) {
    this.function = function;
  }

  public String format(String columnAndAlias) {
    return String.format(function, columnAndAlias);
  }
}
