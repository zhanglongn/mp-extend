package com.zon.len.mp.extend.method;

import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.zon.len.mp.extend.script.ExtendMybatisCommand;
import com.zon.len.mp.extend.script.MybatisCommand;
import java.util.Map;

/**
 * @author ZonLen since on 2021/12/18 下午5:14
 */
public class ExtendJoinSelectOne extends AbstractJoinMethod {


  @Override
  public Class<?> resultType(TableInfo tableInfo) {
    return Map.class;
  }

  @Override
  public MybatisCommand sqlCommandType() {
    return ExtendMybatisCommand.JOIN_SELECT_ONE;
  }
}
