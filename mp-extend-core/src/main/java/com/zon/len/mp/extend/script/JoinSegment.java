package com.zon.len.mp.extend.script;

/**
 * @author ZonLen since on 2021/12/18 下午8:59
 */
public enum JoinSegment {
  /**
   * 左联SQL片段
   */
  LEFT_JOIN(" LEFT JOIN %s ON %s"),
  /**
   * 右联SQL片段
   */
  RIGHT_JOIN(" RIGHT JOIN %s ON %s"),
  /**
   * 内联SQL片段
   */
  INNER_JOIN(" INNER JOIN %s ON %s"),
  ;


  private final String sql;

  JoinSegment(String sql) {
    this.sql = sql;
  }

  public String getSql() {
    return this.sql;
  }

}

