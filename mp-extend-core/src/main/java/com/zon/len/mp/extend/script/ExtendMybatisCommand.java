package com.zon.len.mp.extend.script;

import java.util.stream.Stream;

/**
 * @author ZL since on 2021/12/23 下午3:54
 */
public enum ExtendMybatisCommand implements MybatisCommand {


  /**
   * 查询
   */
  //<script><if test="ew != null and ew.sqlFirst != null">
  //${ew.sqlFirst}
  //</if> SELECT <choose>
  //<when test="ew != null and ew.sqlSelect != null">
  //${ew.sqlSelect}
  //</when>
  //<otherwise>id,user_name,age,creation_time,creator,pubts,modifier,modified_time,dr,ytenant_id</otherwise>
  //</choose> ${ew.joinSelect} FROM ${ew.paramNameValuePairs.userInfo} ${ew.joinSql}
  //<if test="ew != null">
  //<where>
  //<if test="ew.entity != null">
  //<if test="ew.entity.id != null">id=#{ew.entity.id}</if>
  //<if test="ew.entity['userName'] != null"> AND user_name=#{ew.entity.userName}</if>
  //<if test="ew.entity['age'] != null"> AND age=#{ew.entity.age}</if>
  //<if test="ew.entity['creationTime'] != null"> AND creation_time=#{ew.entity.creationTime}</if>
  //<if test="ew.entity['creator'] != null"> AND creator=#{ew.entity.creator}</if>
  //<if test="ew.entity['pubts'] != null"> AND pubts=#{ew.entity.pubts}</if>
  //<if test="ew.entity['modifier'] != null"> AND modifier=#{ew.entity.modifier}</if>
  //<if test="ew.entity['modifiedTime'] != null"> AND modified_time=#{ew.entity.modifiedTime}</if>
  //<if test="ew.entity['dr'] != null"> AND dr=#{ew.entity.dr}</if>
  //<if test="ew.entity['ytenantId'] != null"> AND ytenant_id=#{ew.entity.ytenantId}</if>
  //</if>
  //<if test="ew.joinCondition != null and ew.joinCondition != ''">
  //AND ${ew.joinCondition}
  //</if>
  //<if test="ew.sqlSegment != null and ew.sqlSegment != '' and ew.nonEmptyOfWhere">
  //<if test="ew.nonEmptyOfEntity and ew.nonEmptyOfNormal"> AND</if>
  //${ew.sqlSegment}
  //</if>
  //</where>
  //<if test="ew.sqlSegment != null and ew.sqlSegment != '' and ew.emptyOfWhere">
  // ${ew.sqlSegment}
  //</if>
  //</if> <if test="ew != null and ew.sqlComment != null">
  //${ew.sqlComment}
  //</if>
  //</script>
  JOIN_SELECT_LIST("joinSelectList", "查询满足条件所有数据",
      "<script>%s SELECT %s %s FROM %s %s %s %s\n</script>"),
  JOIN_SELECT_COUNT("joinSelectCount", "查询满足条件的总数",
      "<script>%s SELECT COUNT(1) FROM %s %s %s %s\n</script>"),
  JOIN_SELECT_PAGE("joinSelectPage", "查询满足条件的列表分页",
      "<script>%s SELECT %s %s FROM %s %s %s %s\n</script>"),
  JOIN_SELECT_ONE("joinSelectOne", "查询满足条件的单个对象",
      "<script>%s SELECT %s %s FROM %s %s %s %s \n</script>"),
  SELECT_BY_ID("selectById", "根据ID 查询一条数据", "SELECT %s FROM %s WHERE %s=#{%s} %s"),
  SELECT_BY_MAP("selectByMap", "根据columnMap 查询一条数据", "<script>SELECT %s FROM %s %s\n</script>"),
  SELECT_BATCH_BY_IDS("selectBatchIds", "根据ID集合，批量查询数据",
      "<script>SELECT %s FROM %s WHERE %s IN (%s) %s </script>"),
  SELECT_COUNT("selectCount", "查询满足条件总记录数", "<script>%s SELECT COUNT(1) FROM %s %s %s\n</script>"),
  SELECT_LIST("selectList", "查询满足条件所有数据", "<script>%s SELECT %s FROM %s %s %s %s\n</script>"),
  SELECT_PAGE("selectPage", "查询满足条件所有数据（并翻页）", "<script>%s SELECT %s FROM %s %s %s %s\n</script>"),
  SELECT_MAPS("selectMaps", "查询满足条件所有数据", "<script>%s SELECT %s FROM %s %s %s %s\n</script>"),
  SELECT_MAPS_PAGE("selectMapsPage", "查询满足条件所有数据（并翻页）",
      "<script>\n %s SELECT %s FROM %s %s %s %s\n</script>"),
  SELECT_OBJS("selectObjs", "查询满足条件所有数据", "<script>%s SELECT %s FROM %s %s %s %s\n</script>");


  private final String method;

  private final String desc;

  private final String sql;

  ExtendMybatisCommand(String method, String desc, String sql) {
    this.method = method;
    this.desc = desc;
    this.sql = sql;
  }

  @Override
  public String getMethod() {
    return method;
  }

  public String getDesc() {
    return desc;
  }

  @Override
  public String getSql() {
    return sql;
  }

  public static boolean isJoin(String method) {
    return Stream.of(JOIN_SELECT_COUNT, JOIN_SELECT_LIST, JOIN_SELECT_ONE, JOIN_SELECT_PAGE)
        .map(ExtendMybatisCommand::getMethod)
        .anyMatch(m -> m.equals(method));
  }

  public static boolean isExtendStatement(String statementId) {
    return Stream.of(values())
        .map(ExtendMybatisCommand::getMethod)
        .anyMatch(statement -> statement.equals(statementId));
  }
}
