package com.zon.len.mp.extend.sharding;

import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;
import com.zon.len.mp.extend.exception.MpExtendException;
import com.zon.len.mp.extend.matedata.TableMetadataHelper;

/**
 * @author ZonLen since on 2021/12/19 下午7:38
 */
public abstract class AbstractTableShardingStrategy<T> implements TableShardingStrategy {

  private final Class<?> tableClass;

  public AbstractTableShardingStrategy() {
    tableClass = ReflectionKit
        .getSuperClassGenericType(this.getClass(), AbstractTableShardingStrategy.class, 0);
    if (tableClass == null) {
      throw new MpExtendException(
          "TableShardingStrategy Class[" + this.getClass() + "] has no generic that is table class");
    }
  }

  @Override
  public String shardingTableName(String orgTableName) {
    return TableMetadataHelper.globalFinalTableName(tableClass);
  }
}
