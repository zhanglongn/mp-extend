package com.zon.len.mp.extend.method;

import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.zon.len.mp.extend.script.ExtendMybatisCommand;
import com.zon.len.mp.extend.script.MybatisCommand;
import org.apache.ibatis.mapping.MappedStatement;

/**
 * @author ZonLen since on 2021/12/21 下午8:48
 */
public class ExtendSelectPage extends AbstractExtendMethod {

  @Override
  public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass,
      TableInfo tableInfo) {
    return mappedStatementSupplierForTable(mapperClass, modelClass, tableInfo);
  }

  @Override
  public String sqlFormat(TableInfo tableInfo) {
    return String.format(sqlCommandType().getSql(), sqlFirst(), sqlSelectColumns(tableInfo, true),
        tableNamePlaceHolder(tableInfo.getTableName()), sqlWhereEntityWrapper(true, tableInfo),
        sqlOrderBy(tableInfo), sqlComment());
  }

  @Override
  public MybatisCommand sqlCommandType() {
    return ExtendMybatisCommand.SELECT_PAGE;
  }
}
