package com.zon.len.mp.extend.method;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.toolkit.sql.SqlScriptUtils;
import com.zon.len.mp.extend.matedata.TableMetadata;
import com.zon.len.mp.extend.matedata.TableMetadataHelper;
import com.zon.len.mp.extend.script.ExtendConstant;
import com.zon.len.mp.extend.script.ExtendMybatisCommand;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

/**
 * @author ZonLen since on 2021/12/22 上午9:25
 */
public abstract class AbstractExtendMethod extends AbstractMethod implements ExtendMethod {

  @Override
  public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> tableClass,
      TableInfo tableInfo) {
    return mappedStatementSupplierForOther(mapperClass, tableClass, tableInfo);
  }

  @Override
  public MappedStatement mappedStatementSupplierForOther(Class<?> mapperClass, Class<?> tableClass,
      TableInfo tableInfo) {
    TableMetadataHelper.addTableMetadata(new TableMetadata(tableInfo));
    SqlSource sqlSource = languageDriver
        .createSqlSource(configuration, sqlFormat(tableInfo), tableClass);
    return this
        .addSelectMappedStatementForOther(mapperClass, sqlCommandType().getMethod(), sqlSource,
            resultType(tableInfo));
  }

  @Override
  public MappedStatement mappedStatementSupplierForTable(Class<?> mapperClass, Class<?> tableClass,
      TableInfo tableInfo) {
    TableMetadataHelper.addTableMetadata(new TableMetadata(tableInfo));
    SqlSource sqlSource = languageDriver
        .createSqlSource(configuration, sqlFormat(tableInfo), tableClass);
    return this
        .addSelectMappedStatementForTable(mapperClass, sqlCommandType().getMethod(), sqlSource,
            tableInfo);
  }

  /**
   * EntityWrapper方式获取select where
   *
   * @param newLine 是否提到下一行
   * @param table   表信息
   * @return String
   */
  @Override
  protected String sqlWhereEntityWrapper(boolean newLine, TableInfo table) {
    String sqlScript;
    if (table.isWithLogicDelete()) {
      sqlScript = table.getAllSqlWhere(true, true, WRAPPER_ENTITY_DOT);
      //entity
      sqlScript = SqlScriptUtils
          .convertIf(sqlScript, String.format("%s != null", WRAPPER_ENTITY), true);
//      if (ExtendMybatisCommand.isJoin(sqlCommandType())) {
//        sqlScript += NEWLINE;
//        sqlScript += SqlScriptUtils.convertIf("AND ${" + ExtendConstant.JOIN_WHERE_CONDITION + "}",
//            String.format("%s != null and %s != ''", ExtendConstant.JOIN_WHERE_CONDITION,
//                ExtendConstant.JOIN_WHERE_CONDITION), true);
//      }
      sqlScript += (NEWLINE + table.getLogicDeleteSql(true, true) + NEWLINE);
      String normalSqlScript = SqlScriptUtils
          .convertIf(String.format("AND ${%s}", WRAPPER_SQLSEGMENT),
              String
                  .format("%s != null and %s != '' and %s", WRAPPER_SQLSEGMENT, WRAPPER_SQLSEGMENT,
                      WRAPPER_NONEMPTYOFNORMAL), true);
      normalSqlScript += NEWLINE;
      normalSqlScript += SqlScriptUtils.convertIf(String.format(" ${%s}", WRAPPER_SQLSEGMENT),
          String.format("%s != null and %s != '' and %s", WRAPPER_SQLSEGMENT, WRAPPER_SQLSEGMENT,
              WRAPPER_EMPTYOFNORMAL), true);
      sqlScript += normalSqlScript;
      sqlScript = SqlScriptUtils.convertChoose(String.format("%s != null", WRAPPER), sqlScript,
          table.getLogicDeleteSql(false, true));
      sqlScript = SqlScriptUtils.convertWhere(sqlScript);
    } else {
      sqlScript = table.getAllSqlWhere(false, true, WRAPPER_ENTITY_DOT);
      sqlScript = SqlScriptUtils
          .convertIf(sqlScript, String.format("%s != null", WRAPPER_ENTITY), true);
//      if (ExtendMybatisCommand.isJoin(sqlCommandType())) {
//        sqlScript += NEWLINE;
//        sqlScript += SqlScriptUtils.convertIf("AND ${" + ExtendConstant.JOIN_WHERE_CONDITION + "}",
//            String.format("%s != null and %s != ''", ExtendConstant.JOIN_WHERE_CONDITION,
//                ExtendConstant.JOIN_WHERE_CONDITION), true);
//      }
      sqlScript += NEWLINE;
      sqlScript += SqlScriptUtils.convertIf(String.format(SqlScriptUtils.convertIf(" AND",
          String.format("%s and %s", WRAPPER_NONEMPTYOFENTITY, WRAPPER_NONEMPTYOFNORMAL), false)
              + " \n AND ${%s}", WRAPPER_SQLSEGMENT),
          String.format("%s != null and %s != '' and %s", WRAPPER_SQLSEGMENT, WRAPPER_SQLSEGMENT,
              WRAPPER_NONEMPTYOFWHERE), true);
      sqlScript = SqlScriptUtils.convertWhere(sqlScript) + NEWLINE;
      sqlScript += SqlScriptUtils.convertIf(String.format(" ${%s}", WRAPPER_SQLSEGMENT),
          String.format("%s != null and %s != '' and %s", WRAPPER_SQLSEGMENT, WRAPPER_SQLSEGMENT,
              WRAPPER_EMPTYOFWHERE), true);
      sqlScript = SqlScriptUtils.convertIf(sqlScript, String.format("%s != null", WRAPPER), true);
    }
    return newLine ? NEWLINE + sqlScript : sqlScript;
  }
}
