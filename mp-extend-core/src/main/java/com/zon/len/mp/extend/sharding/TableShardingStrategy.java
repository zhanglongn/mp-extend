package com.zon.len.mp.extend.sharding;

import com.baomidou.mybatisplus.core.toolkit.StringPool;

/**
 * @author ZonLen since on 2021/12/19 下午7:38
 */
public interface TableShardingStrategy {

  default String dataBaseName(){
    return StringPool.EMPTY;
  }

  String shardingTableName(String orgTableName);

}
