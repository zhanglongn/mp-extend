package com.zon.len.mp.extend.configuration;

import static java.util.stream.Collectors.toList;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.AbstractSqlInjector;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.injector.methods.Delete;
import com.baomidou.mybatisplus.core.injector.methods.DeleteBatchByIds;
import com.baomidou.mybatisplus.core.injector.methods.DeleteById;
import com.baomidou.mybatisplus.core.injector.methods.DeleteByMap;
import com.baomidou.mybatisplus.core.injector.methods.Insert;
import com.baomidou.mybatisplus.core.injector.methods.SelectBatchByIds;
import com.baomidou.mybatisplus.core.injector.methods.SelectById;
import com.baomidou.mybatisplus.core.injector.methods.SelectByMap;
import com.baomidou.mybatisplus.core.injector.methods.Update;
import com.baomidou.mybatisplus.core.injector.methods.UpdateById;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.zon.len.mp.extend.method.ExtendJoinSelectCount;
import com.zon.len.mp.extend.method.ExtendJoinSelectList;
import com.zon.len.mp.extend.method.ExtendJoinSelectOne;
import com.zon.len.mp.extend.method.ExtendJoinSelectPage;
import com.zon.len.mp.extend.method.ExtendSelectCount;
import com.zon.len.mp.extend.method.ExtendSelectList;
import com.zon.len.mp.extend.method.ExtendSelectMaps;
import com.zon.len.mp.extend.method.ExtendSelectMapsPage;
import com.zon.len.mp.extend.method.ExtendSelectObjs;
import com.zon.len.mp.extend.method.ExtendSelectPage;
import java.util.List;
import java.util.stream.Stream;

/**
 * 基于MP增强的SQL注入器，主要
 *
 * @see DefaultSqlInjector
 *
 * @author ZonLen since on 2021/12/18 下午10:51
 */
public class ExtendSqlInjector extends AbstractSqlInjector {

  /**
    * 抽象封装MappedStatement生成mybatis的SQL脚本模版，
   *  包含占位符提供给动态SQL自定义组装
    */
  @Override
  public List<AbstractMethod> getMethodList(Class<?> mapperClass, TableInfo tableInfo) {
    if (tableInfo.havePK()) {
      return Stream.of(
          new Insert(),
          new Delete(),
          new DeleteByMap(),
          new DeleteById(),
          new DeleteBatchByIds(),
          new Update(),
          new UpdateById(),
          new SelectById(),
          new SelectBatchByIds(),
          new SelectByMap(),
          new ExtendSelectCount(),
          new ExtendSelectMaps(),
          new ExtendSelectMapsPage(),
          new ExtendSelectObjs(),
          new ExtendSelectList(),
          new ExtendSelectPage(),
          new ExtendJoinSelectCount(),
          new ExtendJoinSelectList(),
          new ExtendJoinSelectOne(),
          new ExtendJoinSelectPage()
      ).collect(toList());
    } else {
      logger.warn(String
          .format("%s ,Not found @TableId annotation, Cannot use Mybatis-Plus 'xxById' Method.",
              tableInfo.getEntityType()));
      return Stream.of(
          new Insert(),
          new Delete(),
          new DeleteByMap(),
          new Update(),
          new SelectByMap(),
          new ExtendSelectCount(),
          new ExtendSelectMaps(),
          new ExtendSelectMapsPage(),
          new ExtendSelectObjs(),
          new ExtendSelectList(),
          new ExtendSelectPage(),
          new ExtendJoinSelectCount(),
          new ExtendJoinSelectList(),
          new ExtendJoinSelectOne(),
          new ExtendJoinSelectPage()
      ).collect(toList());
    }
  }
}
