package com.zon.len.mp.extend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.zon.len.mp.extend.wrapper.LambdaExtendWrapper;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

/**
 * @author ZL since on 2021/12/23 下午4:35
 */
public interface ExtendBaseMapper<T> extends BaseMapper<T> {

  /**
   * 多表查询列表
   *
   * @param wrapper 条件包装
   * @param <E>     适配各种传入类型
   * @return 返回包装类型的对象list
   */
  <E> List<Map<String, Object>> joinSelectList(
      @Param(Constants.WRAPPER) LambdaExtendWrapper<E> wrapper);

  /**
   * 多表查询单个
   *
   * @param wrapper 条件包装
   * @param <E>     传入类型
   * @return 返回包装类型的对象
   */
  <E> Map<String, Object> joinSelectOne(@Param(Constants.WRAPPER) LambdaExtendWrapper<E> wrapper);

  /**
   * 多表查询count
   *
   * @param wrapper 条件包装
   * @param <E>     返回类型
   * @return 总数
   */
  <E> long joinSelectCount(@Param(Constants.WRAPPER) LambdaExtendWrapper<E> wrapper);

  /**
   * 多表查询分页
   *
   * @param page         分页参数
   * @param queryWrapper 条件包装
   * @param <E>          返回类型
   * @param <C>          传入Wrapper类型
   * @return E
   */
  <E extends IPage<Map<String, Object>>, C> E joinSelectPage(E page,
      @Param(Constants.WRAPPER) LambdaExtendWrapper<C> queryWrapper);


}
