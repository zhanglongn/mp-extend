package com.zon.len.mp.extend.method;

import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.zon.len.mp.extend.script.ExtendMybatisCommand;
import com.zon.len.mp.extend.script.MybatisCommand;

/**
 * @author ZonLen since on 2021/12/21 下午8:48
 */
public class ExtendSelectCount extends AbstractExtendMethod {

  @Override
  public String sqlFormat(TableInfo tableInfo) {
    return String.format(sqlCommandType().getSql(), sqlFirst(),
        tableNamePlaceHolder(tableInfo.getTableName()),
        sqlWhereEntityWrapper(true, tableInfo), sqlComment());
  }

  @Override
  public Class<?> resultType(TableInfo tableInfo) {
    return Long.class;
  }

  @Override
  public MybatisCommand sqlCommandType() {
    return ExtendMybatisCommand.SELECT_COUNT;
  }
}
