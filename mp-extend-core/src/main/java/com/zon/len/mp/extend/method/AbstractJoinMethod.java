package com.zon.len.mp.extend.method;

import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.zon.len.mp.extend.script.ExtendConstant;

/**
 * @author ZonLen since on 2021/12/22 上午9:25
 */
public abstract class AbstractJoinMethod extends AbstractExtendMethod {


  @Override
  public String sqlFormat(TableInfo tableInfo) {
    return String
        .format(sqlCommandType().getSql(), sqlFirst(), sqlSelectColumns(tableInfo, true),
            ExtendConstant.JOIN_TABLE_SELECT,
            tableNamePlaceHolder(tableInfo.getTableName()), ExtendConstant.JOIN_SQL_NAME,
            sqlWhereEntityWrapper(true, tableInfo),
            sqlComment());
  }

}
