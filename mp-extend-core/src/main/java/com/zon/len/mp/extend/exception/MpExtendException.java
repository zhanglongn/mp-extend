package com.zon.len.mp.extend.exception;

/**
 * @author ZonLen since on 2021/12/21 上午10:34
 */
public class MpExtendException extends RuntimeException{

  public MpExtendException(String message) {
    super(message);
  }
}
