package com.zon.len.mp.extend.sharding;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author ZL since on 2021/12/18 下午9:02
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
public @interface TableSharding {

  Class<? extends TableShardingStrategy> sharding();

}
