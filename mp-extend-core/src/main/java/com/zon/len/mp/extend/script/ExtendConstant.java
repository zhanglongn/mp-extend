package com.zon.len.mp.extend.script;

import com.baomidou.mybatisplus.core.toolkit.Constants;

/**
 * @author ZL since on 2021/12/23 下午4:36
 */
public final class ExtendConstant {

  private ExtendConstant() {
  }

  /**
   * join SQL片段xml名称
   */
  public static final String JOIN_TABLE_SELECT = "${" + Constants.WRAPPER + ".joinSelect}";

//  /**
//   * join条件 在where上体现
//   */
//  public static final String JOIN_WHERE_CONDITION = Constants.WRAPPER + ".joinCondition";

  /**
   * join SQL片段xml名称
   */
  public static final String JOIN_SQL_NAME = "${" + Constants.WRAPPER + ".joinSql}";

  /**
   * mybatis plus 参数名字
   */
  public static final String MP_PARAMS_NAME = Constants.WRAPPER + ".paramNameValuePairs";

  /**
   * 表名 SQL片段 xml名称
   */
  public static final String TABLE_NAME = "${" + MP_PARAMS_NAME + ".%s}";

}
