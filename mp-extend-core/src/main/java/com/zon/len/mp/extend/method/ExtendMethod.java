package com.zon.len.mp.extend.method;

import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.zon.len.mp.extend.script.ExtendConstant;
import com.zon.len.mp.extend.script.MybatisCommand;
import org.apache.ibatis.mapping.MappedStatement;

/**
 * @author ZonLen since on 2021/12/21 下午8:54
 */
public interface ExtendMethod {

  MappedStatement mappedStatementSupplierForTable(Class<?> mapperClass, Class<?> tableClass,
      TableInfo tableInfo);

  MappedStatement mappedStatementSupplierForOther(Class<?> mapperClass, Class<?> tableClass,
      TableInfo tableInfo);

  default String tableNamePlaceHolder(String originalTableName) {
    return String.format(ExtendConstant.TABLE_NAME, StringUtils.underlineToCamel(originalTableName));
  }

  String sqlFormat(TableInfo tableInfo);

  default Class<?> resultType(TableInfo tableInfo){
    return tableInfo.getEntityType();
  }

  /**
   * sql 脚本
   */
  MybatisCommand sqlCommandType();
}
