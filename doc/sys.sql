/*
 Navicat Premium Data Transfer

 Source Server         : localhost-mysql8
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : sys

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 24/12/2021 15:07:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user_amount
-- ----------------------------
DROP TABLE IF EXISTS `user_amount`;
CREATE TABLE `user_amount` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `creation_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `creator` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'SYS' COMMENT '创建人',
  `pubts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间戳',
  `modifier` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'SYS' COMMENT '更新人',
  `ytenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '~' COMMENT '租户ID',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `dr` bigint NOT NULL DEFAULT '0' COMMENT '删除标识(0:未删除，1:已删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for user_amount1
-- ----------------------------
DROP TABLE IF EXISTS `user_amount1`;
CREATE TABLE `user_amount1` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `creation_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `creator` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'SYS' COMMENT '创建人',
  `pubts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间戳',
  `modifier` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'SYS' COMMENT '更新人',
  `ytenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '~' COMMENT '租户ID',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `dr` bigint NOT NULL DEFAULT '0' COMMENT '删除标识(0:未删除，1:已删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_name` varchar(32) DEFAULT NULL,
  `age` int DEFAULT NULL,
  `creation_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `creator` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'SYS' COMMENT '创建人',
  `pubts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间戳',
  `modifier` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'SYS' COMMENT '更新人',
  `ytenant_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '~' COMMENT '租户ID',
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `dr` bigint NOT NULL DEFAULT '0' COMMENT '删除标识(0:未删除，1:已删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of user_info
-- ----------------------------
BEGIN;
INSERT INTO `user_info` VALUES (1, 'zhanglong', 31, '2021-12-24 12:31:33', 'SYS', '2021-12-24 12:31:33', 'SYS', '~', '2021-12-24 12:31:33', 0);
INSERT INTO `user_info` VALUES (2, 'zhanglong', 31, '2021-12-24 12:31:33', 'SYS', '2021-12-24 12:31:33', 'SYS', '~', '2021-12-24 12:31:33', 0);
INSERT INTO `user_info` VALUES (3, 'zhanglong', 31, '2021-12-24 12:31:33', 'SYS', '2021-12-24 12:31:33', 'SYS', '~', '2021-12-24 12:31:33', 0);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
