package com.zon.len.dynamic.annocation;

import com.zon.len.dynamic.DynamicDruidDataSource.DynamicDataSourceKey;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author ZonLen since on 2021/12/31 下午1:20
 */
@Inherited
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DynamicDataSource {

  /**
   * 默认数据库类型
   */
  DynamicDataSourceKey value() default DynamicDataSourceKey.DEFAULT_DB;
}
