package com.zon.len.dynamic.exception;

/**
 * @author ZonLen since on 2022/1/1 上午10:00
 */
public class DynamicDataSourceException extends RuntimeException{

  public DynamicDataSourceException(String message) {
    super(message);
  }

  public DynamicDataSourceException(String message, Throwable cause) {
    super(message, cause);
  }
}
