package com.zon.len.dynamic.properties;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @author ZonLen since on 2021/12/31 下午3:27
 */
@ConfigurationProperties(prefix = "spring.datasource.druid")
public class MultipleDruidDataSourceProperties extends DruidConfigProperties{

  @Getter
  @NestedConfigurationProperty
  private final List<DynamicDruidDataSourceProperties> dynamicSources = new ArrayList<>();


  public static class DynamicDruidDataSourceProperties extends DruidConfigProperties {

  }

}
