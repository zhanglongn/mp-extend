package com.zon.len.dynamic;

import com.zon.len.dynamic.properties.MultipleDruidDataSourceProperties;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @author ZonLen since on 2021/12/30 下午11:48
 */
@Configuration
@EnableConfigurationProperties({MultipleDruidDataSourceProperties.class,
    DataSourceProperties.class})
public class DynamicDataSourceConfiguration {

  /**
   * 动态数据源配置 必须要关注资源的是初始化和释放; 否则可能导致资源异常
   */
  @Bean
  @Primary
  public DynamicDruidDataSource dataSource(DataSourceProperties dataSourceProperties,
      MultipleDruidDataSourceProperties multipleDruidDataSourceProperties) {
    return new DynamicDruidDataSource(
        multipleDruidDataSourceProperties, dataSourceProperties);
  }

}